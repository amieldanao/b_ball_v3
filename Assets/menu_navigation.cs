﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class menu_navigation : MonoBehaviour
{

	public drills drillsScript;
    public GameObject window;
    public GameObject categories;
    public GameObject videoWindow;
    public GameObject gameWindow;
	public GameObject gameSubMenu;
	public GameObject gameSelectMenu;
	public GameObject equipment;
	public GameObject info;
	public GameObject infoMenu;
	public GameObject rulesPanel;
	public GameObject historyPanel;
	public GameObject healthPanel;
	public VideoPlayer vp;
	public GameObject[] EquipsIcon;
	//public int backIndex;
    
	void Start(){
		drillsScript.menuScript = this;
		
		// drillsScript.playerScripts[0].gameObject.SetActive(false);
		// drillsScript.playerScripts[1].gameObject.SetActive(false);
		// drillsScript.playerScripts[2].gameObject.SetActive(false);
	}

    public void Clicked(GameObject go) {
        switch (go.name) {
            case "btn_categories":
                window.SetActive(false);
                categories.SetActive(true);
				
				constant.BACK_INDEX = 2;
            break;
            case "btn_Game":
                window.SetActive(false);
                gameWindow.SetActive(true);				
				constant.BACK_INDEX = 3;
            break;
			case "btn_drills":
                window.SetActive(false);
                videoWindow.SetActive(true);
				constant.BACK_INDEX = 4;
            break;
			case "btn_Information":
                window.SetActive(false);
                infoMenu.SetActive(true);
				constant.BACK_INDEX = 5;
            break;
			case "basketball_equipments":
                infoMenu.SetActive(false);
                equipment.SetActive(true);
				constant.BACK_INDEX = 6;
            break;
			
			case "rules_and_regulation":
                infoMenu.SetActive(false);
                rulesPanel.SetActive(true);
				constant.BACK_INDEX = 7;
            break;
			case "history_button":
                infoMenu.SetActive(false);
                historyPanel.SetActive(true);
				constant.BACK_INDEX = 8;
            break;
			case "health_button":
                infoMenu.SetActive(false);
                healthPanel.SetActive(true);
				constant.BACK_INDEX = 10;
            break;
			
        }
		
		if (go.name.Contains("Button")){
			constant.BACK_INDEX = 9;
		}
    }
	
	public void backButton()
	{
		Debug.Log(constant.BACK_INDEX);
			switch(constant.BACK_INDEX){
				case 1://from game window submenu
					drillsScript.DemoCam.enabled = false;
					info.SetActive(false);
					drillsScript.mainCam.gameObject.SetActive(true);	
					drillsScript.camSwipe.enabled = false;
					gameSubMenu.SetActive(false);
					gameSelectMenu.SetActive(true);
					drillsScript.anim.Play("default", 0);
					
					// drillsScript.playerScripts[0].gameObject.SetActive(false);
					// drillsScript.playerScripts[1].gameObject.SetActive(false);
					// drillsScript.playerScripts[2].gameObject.SetActive(false);
			
					constant.BACK_INDEX = 2;
				break;
				
				case 2://from categories window
					categories.SetActive(false);
					window.SetActive(true);
					constant.BACK_INDEX = 0;
				break;
				case 3://from game window
					gameWindow.SetActive(false);
					window.SetActive(true);
					constant.BACK_INDEX = 0;
				break;
				
				case 4://from drills window
					vp.Stop();
					videoWindow.SetActive(false);
					window.SetActive(true);
					constant.BACK_INDEX = 0;
				break;
				
				case 5://from info window
					infoMenu.SetActive(false);
					window.SetActive(true);
					constant.BACK_INDEX = 0;
				break;
				
				
				case 6://from equipment window
					equipment.SetActive(false);
					infoMenu.SetActive(true);
					constant.BACK_INDEX = 5;
				break;
				
				case 7://from rules window
					rulesPanel.SetActive(false);
					infoMenu.SetActive(true);
					constant.BACK_INDEX = 5;
				break;
				
				case 8://from history window
					historyPanel.SetActive(false);
					infoMenu.SetActive(true);
					constant.BACK_INDEX = 5;
				break;
				
				case 9://from equip item window
					foreach(GameObject go in EquipsIcon){
						go.SetActive(false);
					}
					constant.BACK_INDEX = 6;
				break;
				
				
				case 10://from health window
					healthPanel.SetActive(false);
					infoMenu.SetActive(true);
					constant.BACK_INDEX = 5;
				break;
				
			}
	}
}
