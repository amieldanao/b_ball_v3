﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class equipment : MonoBehaviour
{
	public GameObject currText;
	public GameObject backButton;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void clickButton(GameObject go){
		if (currText != null)
			currText.SetActive(false);
		currText = go;
		backButton.transform.SetParent(go.transform, false);
		backButton.transform.SetAsLastSibling();
		go.SetActive(true);
	}
}
