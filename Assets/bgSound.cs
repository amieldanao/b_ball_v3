﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class bgSound : MonoBehaviour
{
	public VideoPlayer vp;
	public AudioSource au;
	public Toggle t;

    void Update()
    {
		if (constant.muted == false){
			if (vp.isPlaying && au.volume > 0)
			{
				au.volume = 0;
			}
			if (!vp.isPlaying && au.volume == 0)
			{
				au.volume = 1;
			}
			Debug.Log("sound check");
		}
    }
	
	public void muteSound(){
		constant.muted = !t.isOn;
	
		if (constant.muted == false){
			au.volume = 1;
		}
		else{
			au.volume = 0;
		}
		
		Debug.Log("sound checksss     " + t.isOn);
	}
}
