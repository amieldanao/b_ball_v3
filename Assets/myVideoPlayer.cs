﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class myVideoPlayer : MonoBehaviour
{
	public VideoClip[] allVids;
	public VideoPlayer vp;
	public Animator anim;
	public Slider sl;
	public Text citation;
	
    public void Resume(){
		vp.Play();
		anim.Play("videoIn", 0);
		sl.gameObject.SetActive(true);
	}
    public void Pause(){
		vp.Pause();
		anim.Play("videoOut", 0);
		sl.gameObject.SetActive(false);
	}
    public void Stop(){
		vp.Stop();
		anim.Play("videoOut", 0);
		sl.gameObject.SetActive(false);
	}
	
	public void Play(GameObject go){
		if (!vp.isPlaying){
			int index = int.Parse(go.name);
			vp.clip = allVids[index];
			refreshSlider();
			anim.Play("videoIn", 0);
			vp.Play();
			sl.gameObject.SetActive(true);
			
			if (index >= 5)
				citation.gameObject.SetActive(true);
			else
				citation.gameObject.SetActive(false);
		}
	}
	
	void LateUpdate(){
		//sl.value += (sl.maxValue * (sl.maxValue / 60)) * Time.deltaTime;
	}
	
	public void refreshSlider(){
		sl.value = 0;
		sl.maxValue = vp.clip.frameCount;
	}
	
	public void Seek(){
		
		Debug.Log("seek");
		vp.frame = (long)sl.value;
		if (!vp.isPlaying)
			vp.Play();
	}
}
