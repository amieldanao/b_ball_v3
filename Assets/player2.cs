﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player2 : MonoBehaviour
{
	[HideInInspector]
	public Ball ballScript;
	public bool ikActive = false;
	[HideInInspector]
	public Animator anim;
	public Vector3 startPos, startRot;
	public Transform lookTarget;
	private playerBall playerScript;
	public string currAction;
    // Start is called before the first frame update
    void Start()
    {
		startPos = transform.position;
		startRot = transform.eulerAngles;
		anim = GetComponent<Animator>();
        ballScript = GameObject.FindGameObjectWithTag("Ball").GetComponent<Ball>();
		playerScript = GetComponent<playerBall>();
    }
	
	

    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if(anim) 
		{
			switch(currAction){
				case "sideToSide":
					//if the IK is active, set the position and rotation directly to the goal. 
					if(ikActive) {
						
								// Set the right hand target position and rotation, if one has been assigned
								// Set the look target position, if one has been assigned
								if(lookTarget != null) {
									anim.SetLookAtWeight(1f);
									anim.SetLookAtPosition(lookTarget.position);
								}
							
						
						
							/* if(ballHandOffSet != null) {
							//anim.SetIKPositionWeight(AvatarIKGoal.Head,1);
							anim.SetIKRotationWeight(AvatarIKGoal.Head,1);  
							anim.SetIKPosition(AvatarIKGoal.LeftHand,ballHandOffSet.position);
							anim.SetIKRotation(AvatarIKGoal.LeftHand,ballHandOffSet.rotation);
						}   */              
					}
					//if the IK is not active, set the position and rotation of the hand and head back to the original position
					else {
						anim.SetIKPositionWeight(AvatarIKGoal.LeftHand,0);
						anim.SetIKRotationWeight(AvatarIKGoal.LeftHand,0);
						anim.SetLookAtWeight(0);
					}
				break;
				
				case "chestPass":
					
				break;
				
			}
        }
    }
}
