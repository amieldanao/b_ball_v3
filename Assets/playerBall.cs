﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class playerBall : MonoBehaviour
{
	public Transform ball, ball2, ball_dribble_spot;
	public Transform waist, myRightHand, myLeftHand, returnHand;
	public Vector3 ballOffset;
	[HideInInspector]
	public Rigidbody rb;
	public bool onHand = true;
	public bool bounced;
	[HideInInspector]
	public Ball ballScript;
	public Animator screenAnim;
	[HideInInspector]
	public Vector3 startPos, startRot;
	public int demoLimit = 3;
	public float snapDistance = 0.1f, dribbleForce = 8f;
	[HideInInspector]
	public int demoCount;
	public GameObject offSetGO;
	[HideInInspector]
	public GameObject offSetGO1, offSetGO2;
	[HideInInspector]
	public SkinnedMeshRenderer sk;
	public Vector3[] allOffSet, allRot;
	public float shootForce = 10f;
	[HideInInspector]
	public Transform target;
	private player2 secondScript;
	
	public Transform ballHandOffSet;
	[HideInInspector]
	public Animator anim;
	public player2 player2Script;
	public Vector3[] playerBallOffsets;
	//[HideInInspector]
	public float dribbleForceAdd = 0;
	public Transform sideOffset;
	public float leftHandWeight = 0;
	public drills drillScript;
	public ParticleSystem ps;
	public playerBall myReceiver;
	
	void OnEnable(){
		Debug.Log("OnEnable");
		if (secondScript)
			secondScript.enabled = false;
	}
	
	void OnAnimatorIK(int layerIndex)
    {
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandWeight);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandWeight);
        anim.SetIKPosition(AvatarIKGoal.LeftHand, sideOffset.position);
        anim.SetIKRotation(AvatarIKGoal.LeftHand, sideOffset.rotation);
    }
	
	public void setOffSetRot(int rotIndex)
    {
        if (rotIndex >= 0)
            offSetGO.transform.LookAt(ball_dribble_spot.position);
        else
            offSetGO.transform.LookAt(offSetGO.transform.position + new Vector3(0, -2f, 0));
	}	
	
	public void shoot(float shootForceLocal)
    {
		ball2.gameObject.SetActive(false);
		ball.gameObject.SetActive(true);		
		ball.SetParent(null);
		ball.transform.position = myRightHand.position;
		rb.velocity = Vector3.zero;
		ballScript.shouldBounceBack = false;
		rb.isKinematic = false;
		offSetGO.transform.LookAt(target);
		rb.AddForce(offSetGO.transform.forward * shootForce, ForceMode.Impulse);
	}
    

	void Awake(){
		if (gameObject.name.Equals("guy2"))
		{
			ballOffset = playerBallOffsets[1];
		}
		
		//drillScript = GameObject.Find("GameWindow").GetComponent<drills>();
	}

    // Start is called before the first frame update
    void Start()
    {
		ball_dribble_spot.GetComponent<MeshRenderer>().enabled = false;
		//sk.SetBlendShapeWeight(0, 100f);
		anim = GetComponent<Animator>();
		startPos = transform.position;
		startRot = transform.rotation.eulerAngles;
        myRightHand = anim.GetBoneTransform(HumanBodyBones.RightHand);
        returnHand = myRightHand;
		myLeftHand = anim.GetBoneTransform(HumanBodyBones.LeftHand);
        waist = anim.GetBoneTransform(HumanBodyBones.Chest);
		ballScript = ball.GetComponent<Ball>();
		rb = ball.GetComponent<Rigidbody>();
		offSetGO1 = new GameObject();
		offSetGO1.name = "rightOffSetGO";
		offSetGO1.transform.parent = myRightHand;
		offSetGO1.transform.localPosition = ballOffset;
		offSetGO1.transform.localScale = (Vector3.one);
		offSetGO = offSetGO1;
		offSetGO2 = new GameObject();
		offSetGO2.name = "leftOffSetGO";
		offSetGO2.transform.parent = myLeftHand;
		offSetGO2.transform.localPosition = ballOffset;
		offSetGO2.transform.localScale = (Vector3.one);
		snapOnHand();
		target = GameObject.FindGameObjectWithTag("shootTarget").transform;
		player2Script.lookTarget = anim.GetBoneTransform(HumanBodyBones.Head);
		sideOffset = ball.transform.Find("sideOffSet");
		secondScript = GetComponent<player2>();
		//gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
		/*if (Input.GetKeyDown(KeyCode.Backspace)){
			snapOnHand();
		}*/
		
		if (bounced && ballScript.shouldBounceBack){
			if (Vector3.Distance(returnHand.position, ball.position) <= snapDistance){
				snapOnHand();
			}
		}
		
		if (demoCount >= demoLimit){
			replay();
		}		
    }
	
	public void setHand(int hand){
		if (hand == 0)
			returnHand = myLeftHand;
		if (hand == 1)
			returnHand = myRightHand;
	}
	
	public void snapOnHand(){
		//Debug.Log("snap on hand");
		rb.velocity = Vector3.zero;
		rb.isKinematic = true;
		//ball.gameObject.SetActive(true);
		ball.parent = returnHand;//offSetGO.transform;
		ball.localPosition = ballOffset;
		ballScript.shouldBounceBack = false;
		bounced = false;
		onHand = true;
	}
	
	public void ballOffSet(int bo){
		ballOffset = allOffSet[bo];
	}
	
	public void dribble(int hand){
		GameObject prevOffSetGO = offSetGO;
        prevOffSetGO.transform.LookAt(ball_dribble_spot.position);
		Debug.Log(prevOffSetGO.name);
        if (hand == 0){
			returnHand = myRightHand;
			offSetGO = offSetGO1;
			
		}
		else{
			returnHand = myLeftHand;
			offSetGO = offSetGO2;
		}

		offSetGO.transform.localPosition = ballOffset;
		ball.parent = null;
		ballScript.shouldBounceBack = true;
		rb.isKinematic = false;
        rb.AddForce(prevOffSetGO.transform.forward * (dribbleForce + dribbleForceAdd), ForceMode.Impulse);
        //rb.AddForce(-Vector3.up * dribbleForce, ForceMode.Impulse);
    }	
	
	public void addDemoCount(){
		demoCount ++;
	}
	
	public void replay(){
		if (drillScript.secAnim != anim){
			demoCount = 0;
		/*string animName = anim.GetCurrentAnimatorClipInfo(0)[0].clip.name;		
		anim.Play(animName);*/
		
		
			
			
			transform.position = startPos;
			transform.rotation = Quaternion.Euler(startRot);
			// player2Script.transform.position = player2Script.startPos;
			// player2Script.transform.rotation = Quaternion.Euler(player2Script.startRot);
			snapOnHand();
			screenAnim.Play("fadeScreen");
		
		}
	}
	
	public void setBall2(int state){
		if (state == 0)
			ball2.gameObject.SetActive(false);
		else
			ball2.gameObject.SetActive(true);
			
		Debug.Log("ball2 active : " + state);
	}
	
	public void passToReceiver(){
		myReceiver.ball2.gameObject.SetActive(true);
		myReceiver.anim.Play("Passing the ball");
	}
	
	public void passtheBall(){
		ball.gameObject.SetActive(false);
		ball2.gameObject.SetActive(false);
		myReceiver.anim.Play("catching");
		myReceiver.ball2.gameObject.SetActive(false);
		ps.Emit(1);
		
		/*ball.parent = null;
		ballScript.shouldBounceBack = false;		
		rb.isKinematic = false;
		offSetGO.transform.LookAt(drillScript.playerScripts[2].myRightHand.position);
		rb.AddForce(offSetGO.transform.forward * shootForce, ForceMode.Impulse);*/
		
		//ball.transform.position = ball2.transform.position;
		
		//rb.AddForce(prevOffSetGO.transform.forward * (dribbleForce + dribbleForceAdd), ForceMode.Impulse);
	}
	
	public void setDrillVariation(){
		
		drillScript.setDrillVariation();
	}
}