using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class quiz : MonoBehaviour
{
	
	public AudioClip incorrect, correct;
	public Text msg, title;
	public float timeout = 7f;
	public Transform mainQPanel, backButton;
	public GameObject DoneMessage;
	public Image checkImage, wrongImage;
	public menu_navigation menuScript;
	private int currIndex = 0, itemsCount, correctAnswers;
	private AudioSource audio;
	private string oldStr;
	private Image left, right; 
	private bool skipBack = true;
	public Button instruct;
	public GameObject correctIcon, wrongIcon;
	public Text wrongMsg;
	public bool canSkip = false;
	
	void OnEnable()
	{		
		currIndex = 0;
		skipBack = true;
		beforeReset();
		
		if (itemsCount > 2){
			RandomizeQuestions();
		}
	}

	void Awake(){
		audio = GetComponent<AudioSource>();
		itemsCount = mainQPanel.childCount;
		Text t = DoneMessage.transform.GetChild(0).GetChild(0).GetComponent<Text>();
		oldStr = t.text;
	}
	
	void RandomizeQuestions(){
		for(int i = 0; i < itemsCount-5; i++){//-5 means, dont include the back button & instructions & DoneImage
			GameObject go = mainQPanel.GetChild(i).gameObject;
			mainQPanel.GetChild(i).SetSiblingIndex(Random.Range(0, itemsCount));
			if (go.name != "BackButton" && go.name != "DoneImage" && go.name != "Instructions" && go.name != "wrongMsg" && go.name != "correct")				
				go.SetActive(false);
		}
		
		reParentUI();
		
		Debug.Log("RandomizeQuestions");
		SetQuestion();
		
	}
	
	void SetQuestion(){
		
		
		if (currIndex < itemsCount-5)
		{
			
			GameObject go = mainQPanel.GetChild(currIndex).gameObject;
			go.SetActive(true);
			title.text = go.name;
			left = go.transform.Find("ButtonA").GetComponent<Image>();
			right = go.transform.Find("ButtonB").GetComponent<Image>();
			
			correctIcon.SetActive(false);
			wrongIcon.SetActive(false);
			
			// if (checkImage != null)
				// checkImage.gameObject.SetActive(false);
			// if (wrongImage != null)
				// wrongImage.gameObject.SetActive(false);
			
			//DoneMessage.transform.parent = go.transform;
			if (msg != null)
				msg.transform.parent.gameObject.SetActive(false);
			//whether to switch correct answer letter
			if (Random.value > 0.5f)
			{
				Sprite oldTA = left.sprite;
				left.sprite = right.sprite;
				right.sprite = oldTA;
				answer aa = go.GetComponent<answer>();
				aa.ans = !aa.ans;
			}
			
			
			msg = go.transform.Find("Image").transform.GetChild(0).GetComponent<Text>();
			//msg.transform.parent.gameObject.SetActive(false);
			//checkImage = go.transform.Find("correct").GetComponent<Image>();
			wrongImage = go.transform.Find("wrong").GetComponent<Image>();
		}
	}
	
	public void checkAnswer(GameObject gg){
		 if (instruct.gameObject.activeSelf)
			 return;
		 
		GameObject go = mainQPanel.GetChild(currIndex).gameObject;
		if (correctIcon.activeSelf == false && wrongIcon.activeSelf == false && !IsInvoking("NextQuestion") && !IsInvoking("DoneAllQuestion"))
		{		
			if ((go.GetComponent<answer>().ans == true && gg.name == "ButtonA") || (go.GetComponent<answer>().ans == false && gg.name == "ButtonB")){
				correctIcon.SetActive(true);
				//checkImage.gameObject.SetActive(true);
				audio.PlayOneShot(correct, 1f);
				correctAnswers ++;
				if ((currIndex + 1) < itemsCount-5)
					Invoke("NextQuestion", timeout-2);
				else
					Invoke("DoneAllQuestion", 1);
			}
			else{
				WrongAnswer();
			}
		}
	}
	
	void reParentUI(){
		correctIcon.transform.SetAsLastSibling();
		wrongIcon.transform.SetAsLastSibling();
		DoneMessage.transform.SetAsLastSibling();
		instruct.transform.SetAsLastSibling();
		backButton.SetAsLastSibling();
	}
	
	public void beforeReset(){
		correctAnswers = 0;
		CancelInvoke("DoneAllQuestion");
		CancelInvoke("NextQuestion");
		msg = null;
		currIndex = 0;
		DoneMessage.SetActive(false);
		instruct.gameObject.SetActive(true);
		correctIcon.SetActive(false);
		wrongIcon.SetActive(false);
		
		reParentUI();
		// if (checkImage != null)
			// checkImage.gameObject.SetActive(false);
		// if (wrongImage != null)
			// wrongImage.gameObject.SetActive(false);
		if (skipBack == false)
			menuScript.backButton();
	}
	
	void NextQuestion(){
	
		canSkip = false;
		wrongIcon.SetActive(false);
		correctIcon.SetActive(false);
		if ((currIndex + 1) < itemsCount-5)
			mainQPanel.GetChild(currIndex).gameObject.SetActive(false);
		currIndex ++;
		SetQuestion();
		
	}

	void WrongAnswer(){
		
		audio.PlayOneShot(incorrect, 1f);
		//wrongImage.gameObject.SetActive(true);
		wrongIcon.SetActive(true);
		wrongMsg.text = msg.text;
		//msg.transform.parent.gameObject.SetActive(true);
		
		canSkip = true;
		if ((currIndex + 1) < itemsCount-5){
			//Invoke("NextQuestion", timeout + 2);
		}
		else{
			Invoke("DoneAllQuestion", 5);
		}
	}
	
	public void skip(){
		if (canSkip)
			if ((currIndex + 1) < itemsCount-5){
				NextQuestion();
			}
			else
			{
				CancelInvoke("DoneAllQuestion");
				DoneAllQuestion();
			}
	}
	
	void DoneAllQuestion(){
		Debug.Log("done!!!!");
		wrongImage.gameObject.SetActive(false);
		DoneMessage.SetActive(true);
		if (msg != null)
			msg.transform.parent.gameObject.SetActive(false);
		Text t = DoneMessage.transform.GetChild(0).GetChild(0).GetComponent<Text>();
		
		t.text = oldStr.Substring(0, oldStr.Length) + correctAnswers + "/" + (itemsCount-5);
		skipBack = false;
		
		GameObject go = mainQPanel.GetChild(currIndex).gameObject;
		go.SetActive(false);
		
		Invoke("beforeReset", 10);
	}
}