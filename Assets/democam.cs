﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class democam : MonoBehaviour
{
	public Vector3 offset;
	public GameObject player;
	[HideInInspector]
	playerBall playerScript;

    //[HideInInspector]

    public float limitRotY_down = 20f, limitRotY_up = -34f, theLimitY;
	public float rotateSpeed = 1, limitAdd = 60f;
	[HideInInspector]
	public Transform pivot;
	[HideInInspector]
	public Vector3 startPos, startRot;
	
	void Awake(){
		player = GameObject.FindGameObjectWithTag("Player");
		playerScript = player.GetComponent<playerBall>();
	}
	
	void Start(){
		startPos = transform.position;
		startRot = transform.eulerAngles;
	}
	
    // LateUpdate is called after Update each frame
    void LateUpdate () 
    {
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        pivot.position = player.transform.position + offset;
	}
	
	public void refresh(){
		pivot.position = startPos;
		pivot.rotation = Quaternion.Euler(startRot);
		//pivot.position = player.transform.position + offset;
		transform.LookAt(playerScript.waist.position);		
	}
	
	void Update(){
		
		if (CrossPlatformInputManager.GetAxis("Rat X") != 0){
			Debug.Log(CrossPlatformInputManager.GetAxis("Rat X"));
			transform.RotateAround(playerScript.transform.position, Vector3.up, CrossPlatformInputManager.GetAxis("Rat X") * Time.deltaTime * 60f);
			transform.LookAt(playerScript.waist.position);
		}
		
		float y_control = CrossPlatformInputManager.GetAxis("Rat Y");
		
		if (y_control != 0)
        {
            /*if (y_control > 0)
				theLimitY = Mathf.Min(theLimitY + (Time.deltaTime * limitAdd * y_control), limitRotY_down);
			if (y_control < 0)
				theLimitY = Mathf.Max(theLimitY + (Time.deltaTime * limitAdd * y_control), limitRotY_up);*/

            //if (theLimitY < limitRotY_down && theLimitY > limitRotY_up){
            //transform.RotateAround(playerScript.transform.position, transform.right, y_control * Time.deltaTime * limitAdd);

            //transform.LookAt(playerScript.waist.position);
            //}


            transform.RotateAround(pivot.position, pivot.right, y_control * limitAdd);

            Vector3 angles = transform.eulerAngles;
            angles.x = Mathf.Clamp(angles.x, limitRotY_up, limitRotY_down);
            angles.z = 0;
            transform.eulerAngles = angles;

            //transform.LookAt(playerScript.waist.position);
        }
	}
}