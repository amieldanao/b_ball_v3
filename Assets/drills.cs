﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Cameras;

public class drills : MonoBehaviour
{
	public Animator anim, camAnim, secAnim;
	public GameObject menu, submenu;
	public Dropdown drop;
	public Text description;
	public Camera mainCam, DemoCam;    
	//public democam DemoCam;
	public playerBall player_script;
	public Image camSwipe;
	[HideInInspector]
	public menu_navigation menuScript;
	[HideInInspector]
	public string sliderAction;
	public Slider sliderVariable;
	[HideInInspector]
	player2 player2Script;
    public GameObject ballObject, ballObject2;
    public FreeLookCam freeLookCam;
	public playerBall[] playerScripts;//0 - Ian, 1 - Ivan, 2 - Ivan2
	private Ball theBall, theBall2;
	
    void Update(){
		if (Input.GetKeyDown("space")){
			
			anim.Play("blink_eye", 1);
		}
	}
	
	void Awake(){
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		
		player_script = player.GetComponent<playerBall>();
		anim = player_script.anim;
		player2Script = secAnim.GetComponent<player2>();
		
		ballObject = player_script.ball.gameObject;
		ballObject2 = player_script.ball2.gameObject;
	}
	
	void Start(){
		theBall = ballObject.GetComponent<Ball>();
		theBall2 = ballObject2.GetComponent<Ball>();
		
		//secAnim.gameObject.SetActive(false);
	}
	
	
	public void clicked(Text title){
		menu.SetActive(false);
		submenu.SetActive(true);
		camSwipe.enabled = true;
		drop.ClearOptions();
		mainCam.gameObject.SetActive(false);
		DemoCam.enabled = true;
		constant.BACK_INDEX = 1;
		sliderVariable.gameObject.SetActive(false);
		sliderAction = "";
		bool resetDribbleForceAdd = true;
		//ballObject.SetActive(true);
		player_script.leftHandWeight = 0;
		secAnim.gameObject.SetActive(false);
		drop.gameObject.SetActive(false);
		
		
        switch (title.text){
			case "Basketball dribbling cues":
				
				
				playerScripts[1].gameObject.SetActive(false);
				playerScripts[0].gameObject.SetActive(true);
				playerScripts[2].gameObject.SetActive(false);
				Awake();
			
				ballObject.SetActive(false);
				ballObject2.SetActive(true);
			
				Debug.Log("ball1 : " + ballObject.activeSelf);
			
				drop.gameObject.SetActive(true);
				List<string> m_DropOptions = new List<string> { "forward dribble", "backward dribble", "sideways dribble", "in-place dribble"};
				drop.AddOptions(m_DropOptions);
				description.text = " 1. <b>Eyes forward </b> \n 2. <b>Finger pads </b>\n 3. <b> Push</b> the ball down\n 4. <b> Ball below waist</b>";
				menuScript.info.SetActive(true);
				Vector3 pos = new Vector3(-0.17f, -3.928f, -90.88f);
				Quaternion rot = Quaternion.identity;
				player_script.startPos = pos;
				player_script.startRot = rot.eulerAngles;
				
				
				player_script.demoLimit = 3;
				setDrillVariation();
			break;
			
			
			case "Shooting":
				
				
				playerScripts[0].gameObject.SetActive(false);
				playerScripts[1].gameObject.SetActive(true);
				playerScripts[2].gameObject.SetActive(false);
				Awake();
				freeLookCam.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
				
				ballObject.SetActive(false);
			ballObject2.SetActive(true);
				/* List<string> m_DropOptions2 = new List<string> { "3 points shot"};
				drop.AddOptions(m_DropOptions2); */
				description.text = "3 points shot";//" 1. <b>Eyes forward </b> \n 2. <b>Finger pads </b>\n 3. <b> Push</b> the ball down\n 4. <b> Ball below waist</b>";
				menuScript.info.SetActive(true);
				//DemoCam.transform.localPosition = new Vector3(0.51f, 2.75f, 3.48f);
				sliderVariable.value = 0;
				
				player_script.transform.position = new Vector3(-0.17f, -3.928f, -95.91f);
				player_script.transform.rotation = Quaternion.Euler(0, 175.76f, 0);
				
				//DemoCam.offset = new Vector3(0.51f, 2.75f, 3.48f);
				//DemoCam.player = anim.gameObject;
				//DemoCam.transform.rotation = Quaternion.Euler(new Vector3(22.75f, -168.69f, 0));
				sliderAction = "3pts shoot";
				
				
				setDrillVariation();
				sliderVariable.gameObject.SetActive(true);
				
				
				player_script.demoLimit = 2;
				
			break;
			
			case "Defensive Move Side To Side":		
				
				
				player2Script.currAction = "sideToSide";
				playerScripts[0].gameObject.SetActive(true);
				playerScripts[1].gameObject.SetActive(false);
				playerScripts[2].gameObject.SetActive(false);
				Awake();
				ballObject.SetActive(false);
			ballObject2.SetActive(true);
				/* List<string> m_DropOptions3 = new List<string> { "Defensive Move Side To Side"};
				drop.AddOptions(m_DropOptions3); */
				description.text = "Defensive Move Side To Side";//" 1. <b>Eyes forward </b> \n 2. <b>Finger pads </b>\n 3. <b> Push</b> the ball down\n 4. <b> Ball below waist</b>";
				menuScript.info.SetActive(true);
				Vector3 pos2 = new Vector3(-4.52f, -3.928f, -93.5f);
				// Quaternion rot2 = Quaternion.Euler(new Vector3(0, 175.7f, 0));
				Quaternion rot2 = Quaternion.Euler(new Vector3(0, 90f, 0));
				player_script.startPos = pos2;
				player_script.startRot = rot2.eulerAngles;
				
				//DemoCam.offset = new Vector3(0.51f, 2.75f, 3.48f);
				//DemoCam.player = anim.gameObject;
				//DemoCam.transform.rotation = Quaternion.Euler(new Vector3(22.75f, -168.69f, 0));
				secAnim.transform.parent = player_script.transform;
				secAnim.transform.localPosition = new Vector3(1.25f, 0, -1.06f);
				secAnim.transform.localRotation = Quaternion.Euler(0, 90, 0);
				secAnim.gameObject.SetActive(true);
				secAnim.SetFloat("index", 0);
				secAnim.Play("defending", 0);
				
				player_script.demoLimit = 3;
				//player2Script.ikActive = true;
			break;

            case "Defensive Straight No Stop":
				playerScripts[1].gameObject.SetActive(false);
				playerScripts[0].gameObject.SetActive(true);		
				playerScripts[2].gameObject.SetActive(false);
				Awake();
				ballObject.SetActive(false);
				ballObject2.SetActive(false);
                /* List<string> m_DropOptions4 = new List<string> { "Defensive Straight No Stop" };
                drop.AddOptions(m_DropOptions4); */
                description.text = "Defensive Straight No Stop";//" 1. <b>Eyes forward </b> \n 2. <b>Finger pads </b>\n 3. <b> Push</b> the ball down\n 4. <b> Ball below waist</b>";
                menuScript.info.SetActive(true);

                menuScript.info.SetActive(true);
                Vector3 pos3 = new Vector3(-0.17f, -3.928f, -90.88f);
                Quaternion rot3 = Quaternion.identity;
                player_script.startPos = pos3;
                player_script.startRot = rot3.eulerAngles;

                //DemoCam.offset = new Vector3(0.51f, 2.75f, 3.48f);
                //DemoCam.player = anim.gameObject;
				player_script.demoLimit = 2;
            break;

            case "Fake Shot Break Right":
				
				
				playerScripts[0].gameObject.SetActive(false);
				playerScripts[1].gameObject.SetActive(true);
				playerScripts[2].gameObject.SetActive(false);
				
				Awake();
				ballObject2.SetActive(false);
			ballObject.SetActive(true);
                /* List<string> m_DropOptions5 = new List<string> { "Fake Shot Break Right" };
                drop.AddOptions(m_DropOptions5); */
                description.text = "Fake Shot Break Right";//" 1. <b>Eyes forward </b> \n 2. <b>Finger pads </b>\n 3. <b> Push</b> the ball down\n 4. <b> Ball below waist</b>";
                menuScript.info.SetActive(true);
                //DemoCam.transform.localPosition = new Vector3(0.51f, 2.75f, 3.48f);
                player_script.transform.position = new Vector3(-0.17f, -3.928f, -96.2f);
                player_script.transform.rotation = Quaternion.Euler(0, 175.76f, 0);
				
				
				
                sliderVariable.gameObject.SetActive(false);
				player_script.ball.localPosition = player_script.ballOffset;
                player_script.demoLimit = 1;
				resetDribbleForceAdd = false;
				player_script.dribbleForceAdd = player_script.dribbleForce;
                break;

            case "Pivoting":
				playerScripts[1].gameObject.SetActive(false);
				playerScripts[0].gameObject.SetActive(true);				
				playerScripts[2].gameObject.SetActive(false);
				Awake();
			ballObject.SetActive(true);
				ballObject2.SetActive(false);
                /* List<string> m_DropOptions6 = new List<string> { "Pivoting" };
                drop.AddOptions(m_DropOptions6); */
                description.text = "Pivoting";//" 1. <b>Eyes forward </b> \n 2. <b>Finger pads </b>\n 3. <b> Push</b> the ball down\n 4. <b> Ball below waist</b>";
                menuScript.info.SetActive(true);
                //DemoCam.transform.localPosition = new Vector3(0.51f, 2.75f, 3.48f);
                player_script.transform.position = new Vector3(-0.17f, -3.928f, -96.2f);
                player_script.transform.rotation = Quaternion.Euler(0, 175.76f, 0);
				ballObject.transform.localRotation = Quaternion.identity;
				
                sliderVariable.gameObject.SetActive(true);
                player_script.demoLimit = 1;
				player_script.leftHandWeight = 1;
            break;
			
			case "Passing the ball":	
				player2Script.currAction = "chestPass";
				playerScripts[0].gameObject.SetActive(false);				
				
				
				
				playerScripts[0].GetComponent<player2>().enabled = true;
			
				playerScripts[1].gameObject.SetActive(true);
				Awake();
				
				playerScripts[2].transform.parent = null;
				playerScripts[2].gameObject.SetActive(true);
				
			
				 /* List<string> m_DropOptions7 = new List<string> { "Chest Pass"};
				drop.AddOptions(m_DropOptions7);  */
				description.text = "Chest Pass";//" 1. <b>Eyes forward </b> \n 2. <b>Finger pads </b>\n 3. <b> Push</b> the ball down\n 4. <b> Ball below waist</b>";
				menuScript.info.SetActive(true);
				Vector3 pos7 = new Vector3(8.46f, -4f, -88.85f);
				// Quaternion rot2 = Quaternion.Euler(new Vector3(0, 175.7f, 0));
				Quaternion rot7 = Quaternion.Euler(new Vector3(0, 270f, 0));
				player_script.startPos = pos7;
				player_script.startRot = rot7.eulerAngles;
				
				//secAnim.transform.parent = player_script.transform;
				secAnim.transform.position = new Vector3(4.9f, -3.97f, -88.85f);
				secAnim.transform.rotation = Quaternion.Euler(0, 90, 0);
				secAnim.gameObject.SetActive(true);
				secAnim.SetFloat("index", 0);
				secAnim.Play("catching", 0);
				
				player_script.demoLimit = 3;
				//player2Script.ikActive = true;
			break;
			
        }
		
		
		anim.Play(title.text, 0);
		theBall.playerScript = player_script;
		if (resetDribbleForceAdd)
			player_script.dribbleForceAdd = 0;
		player_script.returnHand = player_script.myRightHand;
		player_script.snapOnHand();
		//player_script.demoLimit = 3;
        
		
		
		
		
		player_script.replay();
		//DemoCam.refresh();
		Invoke("setSlider", 1f);
        freeLookCam.SetTarget(player_script.transform);
    }
	
	void setSlider(){
		
		if ((anim.GetCurrentAnimatorStateInfo(0).IsName("Shooting")) && drop.value == 0 || sliderAction == "3pts shoot")
			sliderVariable.gameObject.SetActive(true);
		else
			sliderVariable.gameObject.SetActive(false);
	}	
	
	public void optionChanged(){
		player_script.replay();
		
		anim.SetFloat("index", drop.value);
		Invoke("setSlider", 1f);
		
		
		if (drop.captionText.text == "in-place dribble"){
			player_script.demoLimit = 1;
		}
	}
	
	public void setDrillVariation()
	{
		int val = (int)sliderVariable.value;
		/*string m_ClipName;
		AnimatorClipInfo[] m_CurrentClipInfo;
		m_CurrentClipInfo = player_script.anim.GetCurrentAnimatorClipInfo(0);
        //Access the Animation clip name
        m_ClipName = m_CurrentClipInfo[0].clip.name;
		
		//string animName = anim.GetCurrentAnimatorClipInfo(0)[0].clip.name;		
		player_script.anim.Play(m_ClipName, 0);*/
		
		
		switch(sliderAction){			
				case "3pts shoot":
					player_script.demoLimit = 3;
					player_script.anim.Play("Shooting");
					Debug.Log("LOOOOOL!");
					if (val == 0){
						Vector3 pos = new Vector3(-0.17f, -3.928f, -95.91f);
						Vector3 rot = new Vector3(0, 175.76f, 0);
						player_script.transform.position = pos;
						player_script.transform.rotation = Quaternion.Euler(rot);
						player_script.target.position = new Vector3(0, 1.443f, -101.76f);
						player_script.target.rotation = Quaternion.Euler(0, -180f, 0);
						player_script.shootForce = 30f;
						player_script.startPos = pos;
						player_script.startRot = rot;
					}
					else if (val == 1){
						player_script.transform.position = new Vector3(6.24f, -3.928f, -103.6f);
						player_script.transform.rotation = Quaternion.Euler(0, -90f, 0);
						player_script.target.position = new Vector3(-1.15f, 2.98f, -103.83f);
						player_script.target.rotation = Quaternion.Euler(0, -180f, 0);
						player_script.shootForce = 27f;
					}
					else if (val == 2){
						player_script.transform.position = new Vector3(-6.47f, -3.928f, -103.6f);
						player_script.transform.rotation = Quaternion.Euler(0, 90f, 0);
						player_script.target.position = new Vector3(0.58f, 2.98f, -103.83f);
						player_script.target.rotation = Quaternion.Euler(0, -180f, 0);
						player_script.shootForce = 27f;
					}
				break;
			case "Basketball dribbling cues":
				
				break;
		}
	}
}
