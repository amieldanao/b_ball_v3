﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	public playerBall playerScript;
	public bool shouldBounceBack;
	public float bounceSpeed = 10f, torque = 7f, hoopReduceVelocity = 5f;
	[HideInInspector]
	public Animator anim;

	void Awake(){
		playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<playerBall>();
		anim = GetComponent<Animator>();
	}

    
    void FixedUpdate()
    {
        if (shouldBounceBack && playerScript.bounced){
			//transform.position = Vector3.MoveTowards(transform.position, playerScript.offSetGO.transform.position, bounceSpeed * Time.deltaTime);
			
		}
    }
	
	void OnCollisionEnter(Collision col){
		//Debug.Log(col.gameObject.name);
		//bounceSpeed = Vector3.Distance(playerScript.lastHandHolding.position, transform.position) * 8f;//playerScript.rb.velocity.magnitude;
		//playerScript.rb.isKinematic = true;
		playerScript.bounced = true;
		
		
	}
	
	void OnTriggerEnter(Collider col){
		//Debug.Log(col.gameObject.tag);
		if (col.gameObject.CompareTag("hoop")){
			//Debug.Log("hoop!");
			playerScript.rb.velocity /= hoopReduceVelocity;//-Vector3.up * bounceSpeed;
			//playerScript.rb.AddForce(Vector3.up * bounceSpeed);
			anim.Play("ballFade", 0);
		}
	}
	
	/*void OnTriggerStay(Collider col){
		if (col.gameObject.CompareTag("hoop")){
			//playerScript.rb.velocity /= 4;//-Vector3.up * bounceSpeed;
			playerScript.rb.AddForce(Vector3.up * bounceSpeed);
		}
	}*/
}
